extends KinematicBody2D
class_name Player

var velocity

var inventory_size := Vector2()

func _physics_process(delta):
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_vector.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	if input_vector != Vector2.ZERO:
		velocity = input_vector * 4
	else:
		velocity = Vector2.ZERO
	
	move_and_collide(velocity)

func _init():
	inventory_size = Vector2(6, 4)
