extends Node

const ICON_PATH = "res://assets/items/"

var items = {
	1: {
		'item_name' : 'sword',
		'tooltip'   : 'sword tooltip',
		'asset'     : ICON_PATH + "sword.png",
		'counter'   : 0,
		'frames'    : {
			'default'   : 0,
			'highlight' : 1
		},
		'slot'       : 'MAIN_HAND',
		'stats_pool' : {}
	}
}

func get_item(item_id):
	if item_id in items:
		return items[item_id]
	else:
		return items["error"]

func increase_counter(item_id):
	items[item_id].counter += 1
