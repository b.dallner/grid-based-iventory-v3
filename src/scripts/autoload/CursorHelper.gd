extends Node

func get_cursor_position_global() -> Vector2:
	return get_node(PathHelper.GAME).get_global_mouse_position()

func get_cursor_position_viewport() -> Vector2:
	return get_viewport().get_mouse_position()

func get_object_under_cursor_from_global():
	var objects = get_tree().get_nodes_in_group("Layer_Global")
	
	return _get_object_under_cursor(objects, self.get_cursor_position_global())

func get_object_under_cursor_from_viewport():
	var objects = get_tree().get_nodes_in_group("Layer_Viewport")
	
	return _get_object_under_cursor(objects, self.get_cursor_position_viewport())

func is_object_under_cursor_global(object):
	return self.is_object_under_position(object, self.get_cursor_position_global())

func is_object_under_cursor_viewport(object):
	return self.is_object_under_position(object, self.get_cursor_position_viewport())

func _get_object_under_cursor(objects, cursor_position):
	for object in objects:
		if(self.is_object_under_position(object, cursor_position)):
			return object
	
	return null

func is_object_under_position(object, cursor_position) -> bool:
	if(object.get_global_rect().has_point(cursor_position)):
		return true
	
	return false
