extends Panel
class_name Item

const STATUS = {
	'ON_FLOOR'     : 1,
	'IN_INVENTORY' : 2,
	'IS_DRAGGED'   : 3,
}

var occupied_cell
var status        : int
var rect

var _frames    := {}
var _id        : int
var _item_name : String
var _layer     : String
var _cell_size : Vector2

func change_status(status:int):
	self.status = status

	if(_layer):
		self.remove_from_group(_layer)
	
	if(status == STATUS.ON_FLOOR):
		_layer = "Layer_Global"
		self.add_to_group(_layer)
	else:
		_layer = ""

func convert_to_cell_size(cell_size:Vector2) -> Vector2:
	if(!_cell_size):
		_calc_cell_size(cell_size)
	
	return _cell_size

func get_anchor_point() -> Vector2:
	if(self.get_rotation_degrees() == 0):
		return self.get_size() / 2
	else:
		return Vector2(self.get_size().y, -self.get_size().x) / 2

func get_offset_top_left_corner(cell_size:Vector2) -> Vector2:
	var offset = Vector2()
	
	if(!_cell_size):
		_calc_cell_size(cell_size)
	
	offset = _cell_size * cell_size / 2
	
	#Subtract half a cell size offset for usability
	offset -= cell_size / 2 
	
	return offset

func init(item_data:Dictionary):
	_id        = item_data.counter 
	_frames    = item_data.frames
	_item_name = item_data.item_name
	
	self.set_name(_item_name + "_" +  str(_id))
	self.add_to_group("Item")
	self.change_status(STATUS.ON_FLOOR)
	
	_load_asset(item_data.asset)

func rotate():
	_cell_size = Vector2(_cell_size.y, _cell_size.x)
	self.set_size(Vector2(self.get_size().y, self.get_size().x))
	
	if($RotationControl.get_rotation_degrees() == 0):
		$RotationControl.set_rotation_degrees(-90)
	else:
		$RotationControl.set_rotation_degrees(0)
		
		

func set_rect_global_position(pos:Vector2):
	self.rect_global_position = pos
	self.rect = rect_global_position

func _calc_cell_size(cell_size:Vector2):
	_cell_size = Vector2(
		clamp(self.get_size().x / cell_size.x, 1, 5),
		clamp(self.get_size().y / cell_size.y, 1, 5)
	)

func _load_asset(asset:String):
	var image = Image.new()
	image.load(asset)
	
	var texture = ImageTexture.new()
	texture.create_from_image(image)
	
	$RotationControl/Sprite.set_texture(texture)
	$RotationControl/Sprite.set_hframes(_frames.size())
	$RotationControl/Sprite.set_frame(_frames.default)
	
#	Set rect_size of item node to match the size of its asset
	self.rect_size = Vector2(texture.get_size().x / _frames.size(), texture.get_size().y)
