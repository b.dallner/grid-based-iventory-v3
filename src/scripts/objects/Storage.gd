extends StaticBody2D
class_name Storage

var _inventory    : Inventory
var _rect_size    : Vector2
var _storage_name : String
var _grid_size    : Vector2

func _init():
	self.add_to_group("Storage")
	self.add_to_group("Layer_Global")
	
	_inventory = load("res://src/scenes/ui/Inventory.tscn").instance()

func _parent_ready():
	_inventory.set_name(_storage_name)
	_inventory.init_grid(_grid_size)
	
	_rect_size = $Sprite.get_rect().size

func store_item(item:Item):
	return _inventory.add_item_picked_up(item)

func get_global_rect() -> Rect2:
	return Rect2(self.get_global_position() - _rect_size / 2, _rect_size)

func toggle_inventory():
	if(get_node(PathHelper.UI).has_node(_inventory.name)):
		get_node(PathHelper.UI).remove_child(_inventory)
		self.show_closed()
	else:
		get_node(PathHelper.UI).add_child(_inventory)
		self.show_open()
		_inventory.show_items()

# Overwrite in inheriting class
func show_open():
	pass

# Overwrite in inheriting class
func show_closed():
	pass
