extends Storage
class_name Chest

func _ready():
	_storage_name = "Chest"
	_grid_size    = Vector2(4, 6)
	
#	Define where the window should be located
	_inventory.center_left()
	
#	_ready function of parent has to be called
	_parent_ready()

# Inherited
func show_open():
	$Sprite.frame = 1

# Inherited
func show_closed():
	$Sprite.frame = 0
