extends CanvasLayer

var _player_inventory : Inventory
var _dragged_item : Item
var _player : Player

var _last_inventory_of_dragged_item
var _last_inventory_under_cursor

func _process(delta):
	if(_dragged_item):
		_dragged_item.set_rect_global_position(
			CursorHelper.get_cursor_position_viewport() -
			_dragged_item.get_anchor_point()
		)
		
		var object = CursorHelper.get_object_under_cursor_from_viewport()
		if(object):
			if(_is_inventory_object(object)):
				object.highlight_cells_under_item(_dragged_item)
				_last_inventory_under_cursor = object
		else:
			_last_inventory_under_cursor.reset_cell_highlighting()

func _input(event):
	if(event.is_class("InputEventMouseButton")):
		if Input.is_action_pressed("left_click"):
			_handle_drag()
		if Input.is_action_just_released("left_click"):
			_handle_click()
	
	if Input.is_action_just_pressed("inventory"):
		_handle_inventory()
	
	if(Input.is_action_just_pressed("rotate")):
		if(_dragged_item):
			_dragged_item.rotate()

func _ready():
	_player           = get_node(PathHelper.PLAYER)
	_player_inventory = load("res://src/scenes/ui/Inventory.tscn").instance()
	
	_player_inventory.set_name("PlayerInventory")
	
	_player_inventory.init_grid(_player.inventory_size)
	_player_inventory.center_right()

func _drop_item(item:Item):
	item.set_global_position(
		_player.get_global_position() -
		item.get_size() / 2
	)
	
	item.change_status(Item.STATUS.ON_FLOOR)
	
	item.occupied_cell = null

func _drop_dragged_item():
	var object_viewport = CursorHelper.get_object_under_cursor_from_viewport()
	var object_global   = CursorHelper.get_object_under_cursor_from_global()
	
	if(object_viewport):
		if(_is_inventory_object(object_viewport)):
			if(!object_viewport.add_item_drag_and_drop(_dragged_item)):
				_return_item_to_previous_inventory(_dragged_item)
	elif(object_global):
		if(_is_storage_object(object_global)):
			if(!object_global.store_item(_dragged_item)):
				_return_item_to_previous_inventory(_dragged_item)
	else:
		_stop_dragging(_dragged_item)
		_drop_item(_dragged_item)
	
	_dragged_item = null

func _handle_click():
	if(!_dragged_item):
		var object_viewport = CursorHelper.get_object_under_cursor_from_viewport()
		var object_global   = CursorHelper.get_object_under_cursor_from_global()
		
		if(object_viewport):
			pass
		elif(object_global):
			if(_is_storage_object(object_global)):
				object_global.toggle_inventory()
			elif(_is_item_object(object_global)):
				_pick_up_item(object_global)
	else:
		_drop_dragged_item()

func _handle_drag():
	var object_viewport = CursorHelper.get_object_under_cursor_from_viewport()
	
	if(object_viewport):
		if(_is_inventory_object(object_viewport)):
			var item:Item = object_viewport.get_item_under_cursor()
			if(item):
				_start_dragging_item(item)

func _handle_inventory():
	if(self.has_node(_player_inventory.name)):
		self.remove_child(_player_inventory)
	else:
		self.add_child(_player_inventory)
		_player_inventory.show_items()

func _is_inventory_object(object):
	return object.is_in_group("Inventory")

func _is_item_object(object):
	return object.is_in_group("Item")

func _is_storage_object(object):
	return object.is_in_group("Storage")

func _pick_up_item(item:Item):
	if(!_player_inventory.add_item_picked_up(item)):
		_drop_item(item)

func _return_item_to_previous_inventory(item:Item):
	_last_inventory_of_dragged_item.add_item_picked_up(item)
	_last_inventory_of_dragged_item = null

func _start_dragging(object):
	object.get_parent().remove_child(object)
	get_node(PathHelper.UI).add_child(object)

func _start_dragging_item(item:Item):
	_last_inventory_of_dragged_item = item.get_parent()
	_start_dragging(item)
	item.change_status(Item.STATUS.IS_DRAGGED)
	
	_dragged_item = item

func _stop_dragging(object):
	_dragged_item.get_parent().remove_child(_dragged_item)
	get_node(PathHelper.GAME).add_child(_dragged_item)

# TODO Temporären code wieder entfernen
var _rows = 4

func _on_Button_pressed():
	if(_player_inventory.change_grid_size(Vector2(6, _rows + 1))):
		_rows += 1


func _on_Button2_pressed():
	if(_player_inventory.change_grid_size(Vector2(6, _rows - 1))):
		_rows -= 1
