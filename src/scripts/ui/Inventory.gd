extends NinePatchRect
class_name Inventory

var _cell_preload = preload("res://src/scenes/ui/Cell.tscn");
var _cell_size   := Vector2()
var _grid_cells  := {}
var _items       := []

var _grid_size := Vector2()

func _init():
	var cell   = _cell_preload.instance()
	_cell_size = cell.get_size() #rect_size
	
	self.add_to_group("Inventory")
	self.add_to_group("Layer_Viewport")

func _ready():
	_draw_grid()

func init_grid(grid_size:Vector2):
	_grid_size = grid_size
	
	_prepare_grid()

func add_item_picked_up(item:Item) -> bool:
	var item_cell_size = item.convert_to_cell_size(_cell_size)
	var free_cell      = _find_first_suitable_cell(item_cell_size)
	
	if(free_cell != null):
		_add_item(item, free_cell, item_cell_size)
		return true
	
	return false

func add_item_drag_and_drop(item:Item) -> bool:
	var free_cell      = null
	var item_cell_size = item.convert_to_cell_size(_cell_size)
	var drop_grid_position = _get_cell_by_position(
			CursorHelper.get_cursor_position_viewport() - item.get_offset_top_left_corner(_cell_size)
		)
	
	if(drop_grid_position == null):
		drop_grid_position = _get_cell_by_position(CursorHelper.get_cursor_position_viewport())
	
	if(_is_required_cell_space_available(drop_grid_position, item_cell_size)):
		free_cell = drop_grid_position
	else:
		free_cell = _find_first_suitable_cell(item_cell_size)
	
	if(free_cell != null):
		_add_item(item, free_cell, item_cell_size)
		return true
	
	return false

func center_left():
	self.anchor_top    = .5
	self.anchor_bottom = .5
	
	self.margin_left = 10
	
	self.grow_vertical   = Control.GROW_DIRECTION_BOTH

func center_right():
	self.anchor_left   = 1
	self.anchor_top    = .5
	self.anchor_right  = 1
	self.anchor_bottom = .5

	self.margin_right = -10
	
	self.grow_horizontal = Control.GROW_DIRECTION_BEGIN
	self.grow_vertical   = Control.GROW_DIRECTION_BOTH

func change_grid_size(new_grid_size:Vector2) -> bool:
	var previous_grid_size = _grid_size
	var items              = _items.duplicate()
	var apply_size_change  = true
	
	init_grid(new_grid_size)
	
	for item in items:
		var item_cell_size = item.convert_to_cell_size(_cell_size)
		var free_cell      = null
		
		if(_is_required_cell_space_available(item.occupied_cell, item_cell_size)):
			free_cell = item.occupied_cell
		else:
			free_cell = _find_first_suitable_cell(item_cell_size)
			
		if(free_cell != null):
			_items.remove(_items.find(item))
			_add_item(item, free_cell, item_cell_size)
		else:
#			Reset grid size to previus size
			self.change_grid_size(previous_grid_size)
			apply_size_change = false
			break
	
	if(apply_size_change):
		_grid_size = new_grid_size
	
		if(self.is_inside_tree()):
			_draw_grid()
	
	return apply_size_change

func get_item_under_cursor():
#	Items are located in the canvas layer (overlay) and there global positions 
#	are relative to the viewport not to the game positions
	for item in _items:
		if(CursorHelper.is_object_under_cursor_viewport(item)):
			_reset_occupied_cells_by_item(item)
			_items.remove(_items.find(item))
			return item
	
	return null

func highlight_cells_under_item(item:Item):
	var item_cell_size  = item.convert_to_cell_size(_cell_size)
	var origin_position = null
	
	var top_left_position = CursorHelper.get_cursor_position_viewport() - item.get_offset_top_left_corner(_cell_size)
	
	var origin_cell = _get_cell_by_position(top_left_position)
	
	if(origin_cell == null):
		origin_cell = _get_cell_by_position(CursorHelper.get_cursor_position_viewport())
	
	for y in range (int(_grid_size.y)):
		for x in range (int(_grid_size.x)):
			_grid_cells[y][x].reset_highlight()
	
#	TODO kann in die obere schleife integriert werden
	for y in range(origin_cell.y, origin_cell.y + item_cell_size.y):
		for x in range(origin_cell.x, origin_cell.x + item_cell_size.x):
			if(_grid_cells.has(y) and _grid_cells[y].has(x)):
					_grid_cells[y][x].highlight()
	return
#
#	for y in range (int(_grid_size.y)):
#		for x in range (int(_grid_size.x)):
#			_grid_cells[y][x].reset_highlight()
#
#			if(CursorHelper.is_object_under_position(_grid_cells[y][x], top_left_position)
#				and !_grid_cells[y][x].is_occupied
#				and _grid_cells.size() >= y + item_cell_size.y
#				and _grid_cells[y].size() >= x + item_cell_size.x
#			):
#				origin_position = Vector2(x, y)
#
#	if(origin_position == null):
#		origin_position = _find_first_suitable_cell(item_cell_size)
#
#	if(origin_position != null):
#		for y in range(origin_position.y, origin_position.y + item_cell_size.y):
#			for x in range(origin_position.x, origin_position.x + item_cell_size.x):
#				if(_grid_cells.has(y) and _grid_cells[y].has(x)):
#					_grid_cells[y][x].highlight()

func reset_cell_highlighting():
	for y in range (int(_grid_size.y)):
		for x in range (int(_grid_size.x)):
			_grid_cells[y][x].reset_highlight()

func show_item(item:Item):
	if(!item.is_inside_tree()):
		self.add_child(item)
		item.set_rect_global_position(self.rect_global_position + item.occupied_cell * _cell_size)

func show_items():
	for item in _items:
		self.show_item(item)

func _convert_position_to_grid_position(pos:Vector2):
	return Vector2(
		int(pos.x / _cell_size.x), 
		int(pos.y / _cell_size.y)
	)

func _add_item(item:Item, free_cell:Vector2, item_cell_size:Vector2):
	_items.append(item)
	_update_grid_cell_occupation(free_cell, item_cell_size, true)
	
	item.change_status(Item.STATUS.IN_INVENTORY)
	
	item.occupied_cell = free_cell
	
	self.reset_cell_highlighting()
	
#	TODO Wirft einen Fehler wenn items direkt in Inventare gespawnt werden
	item.get_parent().remove_child(item)
	
	if(self.is_inside_tree()):
		self.show_item(item)

func _draw_grid():
	for child in $GridContainer.get_children():
		$GridContainer.remove_child(child)
	
	$GridContainer.set_columns(int(_grid_size.x))
	for y in range (int(_grid_size.y)):
		for x in range (int(_grid_size.x)):
			$GridContainer.add_child(_grid_cells[y][x])
	
#	TODO Zentrierung funktioniert noch nicht wenn sich die grid size reduziert
	self.set_custom_minimum_size(_grid_size * _cell_size)

# Find the first not occupied cell with a suitable surrounding
func _find_first_suitable_cell(item_cell_size:Vector2):
	for y in range(_grid_size.y):
		for x in range(_grid_size.x):
			if(!_grid_cells[y][x].is_occupied):
				if(_is_required_cell_space_available(Vector2(x, y), item_cell_size)):
					return Vector2(x,y)
	return null

func _get_cell_by_position(pos:Vector2):
	for y in range (int(_grid_size.y)):
		for x in range (int(_grid_size.x)):
			if(CursorHelper.is_object_under_position(_grid_cells[y][x], pos)):
				return Vector2(x, y)
	
	return null

# Check if any of the required cells is already occupied. If the required cells
# are not occupied by an item, return true. If one of them is occupied by an 
# item, return false. If the required space leaks over the grid boundry
# return false
func _is_required_cell_space_available(target_cell:Vector2, item_cell_size:Vector2) -> bool:
	for y in range(target_cell.y, target_cell.y + item_cell_size.y):
		if(y >= _grid_size.y):
			return false
		for x in range(target_cell.x, target_cell.x + item_cell_size.x):
			if(x >= _grid_size.x or _grid_cells[y][x].is_occupied):
				return false
	
	return true

func _prepare_grid():
	for y in range (_grid_size.y):
		_grid_cells[y] = {}
		for x in range (_grid_size.x):
			var cell = _cell_preload.instance()
			
			cell.set_custom_minimum_size(_cell_size)
			cell.set_name("Cell_" + str(x) + "_" + str(y))
			
			_grid_cells[y][x] = cell

func _reset_occupied_cells_by_item(item:Item):
	var item_cell_size = item.convert_to_cell_size(_cell_size)
	_update_grid_cell_occupation(item.occupied_cell, item_cell_size, false)

func _update_grid_cell_occupation(free_cell:Vector2, item_cell_size:Vector2, status:bool):
	for y in range(free_cell.y, free_cell.y + item_cell_size.y):
		for x in range(free_cell.x, free_cell.x + item_cell_size.x):
			 _grid_cells[y][x].is_occupied = status
