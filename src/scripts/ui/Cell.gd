extends Control
class_name Cell

var is_occupied = false

func highlight():
	$Sprite.modulate = Color(1, 1, 1, .8)

func reset_highlight():
	$Sprite.modulate = Color(.8, .8, .8, .8)
