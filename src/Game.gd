extends Control

var _item_preload = preload("res://src/scenes/items/Item.tscn")

func _ready():
#	TODO Temporären code wieder entfernen
	_spawn_chest()
#	_spawn_item(1)
	_spawn_item(1)
#	-------------------------------------------

func _spawn_chest():
	var chest:Chest = load("res://src/scenes/objects/Chest.tscn").instance()
	self.add_child(chest)

func _spawn_item(item_id:int):
	var item:Item = _item_preload.instance()
	item.init(ItemDB.get_item(item_id))
	ItemDB.increase_counter(item_id)
	
	self.add_child(item)
	item.set_position(_randomize_position(item.get_position()))

func _randomize_position(pos:Vector2) -> Vector2:
	pos.x += rand_range(-250, 250)
	pos.y += rand_range(-250, 250)
	
	return pos

# TODO Temporären code wieder entfernen
func _on_Button3_pressed():
	_spawn_item(1)
